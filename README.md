# Readme

## How to

### run locally:
```shell
./mvnw spring-boot:run -Dspring-boot.run.profiles=local

```
Then you can go to http://localhost:8080/

### build:
```shell
./mvnw clean install
```

### deploy:
```shell
 ./mvnw package appengine:deploy

```

```shell
gcloud app browse
```

