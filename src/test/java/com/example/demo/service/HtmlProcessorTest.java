package com.example.demo.service;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class HtmlProcessorTest {

    @InjectMocks
    HtmlProcessor processor;

    @Mock
    S3Service service;

    @Test
    void process() throws IOException {

        String sourceHtml = new String(this.getClass().getResourceAsStream("/test.html").readAllBytes());
        String result = processor.process(sourceHtml);
//        assertFalse(result.contains("\"/data/")); //html is still there
        assertFalse(result.contains("\"/meta-info/icons/"));
    }
}