package com.example.demo.service;

import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SearchServiceTest {

    public SearchService searchService = new SearchService();

    @Test
    void normilizeRequestPath() {
        assertEquals("data/moe",
        searchService.normilizeRequestPath("/data/moe/"));
        assertEquals("data/moe",
                searchService.normilizeRequestPath("/data/moe"));
        assertEquals("data/moe",
                searchService.normilizeRequestPath("data/moe/"));
        assertEquals("data/moe",
                searchService.normilizeRequestPath("data/moe"));
        assertEquals("data/моє",
                searchService.normilizeRequestPath("data/моє"));
    }

    @Test
    void normalizeRequestString(){
        assertEquals(Sets.set("Data", "Моє"),
                searchService.normalizeRequestString("Data, моє "));
        assertEquals(Sets.set("Data", "Моє"),
                searchService.normalizeRequestString("data, моє "));
        assertEquals(Sets.set("Data", "Моє"),
                searchService.normalizeRequestString(" data , Моє "));
    }
}