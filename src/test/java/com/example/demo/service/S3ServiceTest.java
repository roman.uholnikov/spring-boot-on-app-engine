package com.example.demo.service;

import org.junit.jupiter.api.Test;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;

import static org.junit.jupiter.api.Assertions.*;

class S3ServiceTest {

    S3Service service;

    @Test
    void createSignedUrl() {
        S3Client s3 = S3Client.builder()
                .credentialsProvider(ProfileCredentialsProvider.builder().profileName("personal1").build())
                .region(Region.EU_CENTRAL_1)
                .build();

        S3Presigner presigner = S3Presigner.builder()
                .credentialsProvider(ProfileCredentialsProvider.builder().profileName("personal1").build())
                .region(Region.EU_CENTRAL_1)
                .build();

        service = new S3Service(s3, "eu-central-1", "cloud-disk-ruholnikov-bucket", null, null, presigner );

        String signedUrl = service.createSignedUrl("data/index.html");
        String signedUrl1 = service.createSignedUrl("data/мое/index.html");
        System.out.println(signedUrl);
        System.out.println(signedUrl1);
    }
}