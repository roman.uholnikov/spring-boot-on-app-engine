package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SearchService {

    @Autowired
    DdbService ddbService;

    public String normilizeRequestPath(String requestPath){
        String result = requestPath.trim();
        if(result.startsWith("/")){
            result = result.substring(1);
        }
        if(result.endsWith("/")){
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public Set<String> normalizeRequestString(String searchRequest){
        Set<String> collect = Arrays.stream(searchRequest.split(","))
                .filter(s -> !s.isBlank())
                .filter(s -> s.length() > 1)
                .map(String::trim)
                .map(String::toLowerCase)
                .map(str -> Arrays.stream(str.split(" "))
                        .map(String::trim)
                        .map(word -> word.substring(0, 1).toUpperCase() + word.substring(1))
                        .reduce("", (a, b) -> a + " " + b)
                        .trim())
                .collect(Collectors.toSet());
        return collect;
    }

    public Map<String, Long> getLabels(String rowSearchQuery){
        return ddbService.getLabels(normilizeRequestPath(rowSearchQuery));
    }

    public Map<String, Set<String>>getFiles(String rowSearchQuery, String searchRequest){
        return ddbService.getFiles(normilizeRequestPath(rowSearchQuery),
                normalizeRequestString(searchRequest));
    }
}
