package com.example.demo.service;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.QueryRequest;
import software.amazon.awssdk.services.dynamodb.model.QueryResponse;
import software.amazon.awssdk.services.dynamodb.model.ReturnConsumedCapacity;
import software.amazon.awssdk.utils.Pair;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DdbService {
    DynamoDbClient dbClient;
    @Value("${aws.access.key}")
    String awsKeyId;
    @Value("${aws.secret.key}")
    String awsKeySecret;
    @Value("${aws.ddb.disk.region}")
    String regionName;
    @Value("${aws.ddb.table.images-labels.name}")
    String tableName;
    @Value("${aws.ddb.table.images-labels.index.name}")
    String tableIndexName;

    public DdbService() {
    }

    public DdbService(DynamoDbClient dbClient, String regionName, String tableName, String tableIndexName) {
        this.dbClient = dbClient;
        this.regionName = regionName;
        this.tableName = tableName;
        this.tableIndexName = tableIndexName;
    }

    @PostConstruct
    public void init() {
        dbClient = DynamoDbClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(
                        AwsBasicCredentials.create(
                                awsKeyId,
                                awsKeySecret)
                ))
                .region(Region.of(regionName))
                .build();
    }


    @Cacheable("searchFilesByLabelsInFolder")
    public Map<String, Set<String>> getFiles(String folder, Collection<String> labels) {
        if(labels.isEmpty()){
            return null;
        }
        Map<String, Set<String>> collected = labels.stream()
                .map(s -> queryTable(folder, s).entrySet())
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey, // Key is the s3 key
                        Collectors.flatMapping(
                                t -> t.getValue().stream(),
                                Collectors.toSet()
                        )
                ));
        return collected.entrySet()
                .stream()
                .filter(e -> {
                    for(String label :labels){
                        if(!e.getValue().contains(label)){
                            return false;
                        }
                    }
                    return true;
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Cacheable("searchLabelsInFolder")
    public Map<String, Long> getLabels(String folderPath) {
        return queryIndex(folderPath);
    }


    /**
     * return file names that are filtered by folder and labe
     *
     * @param folder
     * @param label
     * @return file-key and its label
     */
    private Map<String, Set<String>> queryTable(String folder, String label) {

        String partition_key_name = "val";
        String filter_key_name = "num";

        HashMap<String, AttributeValue> attrValues = new HashMap<>();
        attrValues.put(":" + partition_key_name, AttributeValue.builder().s(label).build());
        attrValues.put(":" + filter_key_name, AttributeValue.builder().s(folder).build());


        QueryRequest queryRequest = QueryRequest.builder()
                .tableName(tableName)
                .keyConditionExpression("id = :" + partition_key_name + " AND begins_with (#s3key, :"+ filter_key_name + ")")
//                .filterExpression("begins_with (#s3key, :"+ filter_key_name + ")")
                .expressionAttributeValues(attrValues)
                .expressionAttributeNames(Map.of(
                        "#s3key", "s3-key" // Map the placeholder to the actual attribute name
                ))
                .returnConsumedCapacity(ReturnConsumedCapacity.TOTAL)
                .build();

        QueryResponse query = dbClient.query(queryRequest);
        log.info("search table result, consumed :{}", query.consumedCapacity());
        return query
                .items()
                .stream()
                .filter(attributeValueMap -> attributeValueMap.get("s3-key").s().startsWith(folder)) //do I need it if I did it on the DDB level?
                .map(aMap -> Pair.of(aMap.get("s3-key").s(),
                        aMap.get("id").s()))
                .collect(Collectors.groupingBy(
                        Pair::left,
                        Collectors.mapping(
                                Pair::right,
                                Collectors.toSet()
                        )
                ));
    }

    /**
     * Return all labels in the given folder
     * @param keyValue - folderPath
     * @return label and its count in result
     */
    private Map<String, Long> queryIndex(String keyValue){
        String partition_key_name = "val";

        HashMap<String, AttributeValue> attrValues = new HashMap<>();
        attrValues.put(":" + partition_key_name, AttributeValue.builder().s(keyValue).build());


        QueryRequest queryRequest = QueryRequest.builder()
                .tableName(tableName)
                .indexName(tableIndexName)
                .keyConditionExpression("#s3key = :" + partition_key_name)
                .expressionAttributeNames(Map.of(
                        "#s3key", "s3-key-path" // Map the placeholder to the actual attribute name
                ))
                .expressionAttributeValues(attrValues)
                .returnConsumedCapacity(ReturnConsumedCapacity.TOTAL)
                .build();

        QueryResponse query = dbClient.query(queryRequest);
        log.info("search index result, consumed :{}", query.consumedCapacity());
        return query
                .items()
                .stream()
                .flatMap(map -> map.entrySet().stream())
                .filter(entry ->
                        entry.getKey().equalsIgnoreCase("id")) //label has attributeName "id"
                .map(entry -> entry.getValue().s())
                .collect(Collectors.groupingBy(
                        Function.identity(), // Key is the label itself
                        Collectors.counting() // Value is the count of occurrences
                ));
    }
}
