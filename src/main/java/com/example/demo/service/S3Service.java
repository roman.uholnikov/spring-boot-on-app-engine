package com.example.demo.service;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;

@Service
@Slf4j
public class S3Service {
    S3Client s3;
    @Value("${aws.s3.disk.region}")
    String regionName;
    @Value("${aws.s3.disk.bucket}")
    String bucketName;
    @Value("${aws.access.key}")
    String awsKeyId;
    @Value("${aws.secret.key}")
    String awsKeySecret;
    S3Presigner presigner;

    public S3Service() {
    }

    public S3Service(S3Client s3, String regionName, String bucketName, String awsKeyId, String awsKeySecret, S3Presigner presigner) {
        this.s3 = s3;
        this.regionName = regionName;
        this.bucketName = bucketName;
        this.awsKeyId = awsKeyId;
        this.awsKeySecret = awsKeySecret;
        this.presigner = presigner;
    }

    @PostConstruct
    public void init() {
        s3 = S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(
                        AwsBasicCredentials.create(
                                awsKeyId,
                                awsKeySecret)
                ))
                .region(Region.of(regionName))
                .build();

        presigner = S3Presigner.builder()
                .credentialsProvider(StaticCredentialsProvider.create(
                        AwsBasicCredentials.create(
                                awsKeyId,
                                awsKeySecret)
                ))
                .region(Region.of(regionName))
                .build();
    }


    @Cacheable("files")
    public String objectAsString(String key) {
        GetObjectRequest objectRequest = GetObjectRequest
                .builder()
                .bucket(bucketName)
                .key(URLDecoder.decode(key, StandardCharsets.UTF_8))
                .build();

        ResponseBytes<GetObjectResponse> objectAsBytes = s3.getObjectAsBytes(objectRequest);
        return objectAsBytes.asUtf8String();
    }

    public String createSignedUrl(String keyName){
        String keyValidated = keyName;
        if(keyName.startsWith("/")){
            keyValidated = keyName.substring(1);
        }
        GetObjectRequest objectRequest = GetObjectRequest.builder()
                .bucket(bucketName)
                .key(keyValidated)
                .build();

        GetObjectPresignRequest presignRequest = GetObjectPresignRequest.builder()
                .signatureDuration(Duration.ofMinutes(10))  // The URL will expire in 10 minutes.
                .getObjectRequest(objectRequest)
                .build();

        PresignedGetObjectRequest presignedRequest = presigner.presignGetObject(presignRequest);
        log.debug("Presigned URL: [{}]", presignedRequest.url().toString());
        log.debug("HTTP method: [{}]", presignedRequest.httpRequest().method());

        return presignedRequest.url().toExternalForm();
    }
}
