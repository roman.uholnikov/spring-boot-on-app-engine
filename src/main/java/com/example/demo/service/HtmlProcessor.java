package com.example.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Service that helps automate work with Existing HTML:
 * transform relative urls to S3 signed URLs,
 */
@Service
@Slf4j
public class HtmlProcessor {
    public static final String REGEX = "\"(/meta-info/icons/[^\"]+)\"|\"(/data/[^\"]+)\"";
    S3Service s3Service;

    Pattern pattern = Pattern.compile(REGEX);

    @Autowired
    public HtmlProcessor(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    /**
     * Takes original HTML and transform relative urls to S3 signed URLs
     * @param html
     * @return updated HTML
     */
    public String process(String html) {
        Matcher matcher = pattern.matcher(html);
        String[] split = html.split(REGEX);
        StringBuilder stringBuilder = new StringBuilder(html.length() * 2);
        for (int i = 0; matcher.find() ; i++) {
            String str = split[i];
            stringBuilder.append(str);
            String group = matcher.group();
            if(group.endsWith(".html\"")) {
                stringBuilder.append(group);
            }
            else {
                stringBuilder.append("\"");
                stringBuilder.append(s3Service.createSignedUrl(group.substring(1, group.length() - 1)));
                stringBuilder.append("\"");
            }
        }
        stringBuilder.append(split[split.length - 1]);

        return substituteCss(stringBuilder.toString());
    }

    private String substituteCss(String string) {
//        return string;
        return string
                .replace("/meta-info/baguetteBox.min.js", "https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.js")
                .replace("/meta-info/baguetteBox.min.css","https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.css");
//                .replace("/meta-info/bootstrap.min.css","https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css");
    }

    /**
     * Search results
     * @param files
     * @param labels
     * @return
     */
    public String getSearchResultHtml(Map<String, Set<String>> files, Map<String, Long> labels, String path, String searchRequest) throws IOException {

        String resourceFileAsString = new String(getClass().getResourceAsStream("/templates/search.html").readAllBytes())
                .replace("SEARCHREQUEST", searchRequest)
                .replace("SEARCHPATH", path);
        String htmlWithLabels = resourceFileAsString.replaceAll("LABELSHERE", getLabelsHtml(labels, path));
        String htmlWithSearchResultsAndLabels = htmlWithLabels.replace("SEARCHRESULTS", getResultHtml(files, path));
        return process(htmlWithSearchResultsAndLabels);
    }

    public String getResultHtml(Map<String, Set<String>> files, String path){
        String template = """
                             <div class="col-sm-3 col-md-2">
                               <a class="lightbox" href="/FILEPATH">  <!--image itself -->
                                 <img src="/meta-info/icons/FILEICONPATH" alt="IMAGEALT"> <!--preview file -->
                               </a>
                            </div>
                """;
        StringBuilder stringBuilder = new StringBuilder();
        if (files.isEmpty()){
            return "no search results";
        }
        for (Map.Entry<String, Set<String>> entry : files.entrySet()) {
            String replace = template
                    .replaceAll("FILEPATH", entry.getKey())
                    .replace("FILEICONPATH", entry.getKey().substring(5))
                    .replace("IMAGEALT", entry.getValue().toString());
            stringBuilder.append(replace);
        }
        return stringBuilder.toString();
    }

    public String getLabelsHtml(Map<String, Long> labels, String path){
        String template = """
                            <div class="none">
                                <a href="/search?search-path=requestPath&search-request=labelName">  <!--folder itself -->
                                    labelName
                                </a>
                            </div>
                """;
        StringBuilder stringBuilder = new StringBuilder();
        if (labels.isEmpty()){
            return "no labels in the folder";
        }
        for (Map.Entry<String, Long> entry : labels.entrySet()) {
            String replace = template
                    .replaceAll("labelName", entry.getKey())
                    .replace("requestPath", path);
            stringBuilder.append(replace);
        }
        return stringBuilder.toString();
    }


    /**
     * Reads given resource file as a string.
     *
     * @param fileName path to the resource file
     * @return the file's contents
     * @throws IOException if read fails for any reason
     */
    static String getResourceFileAsString(String fileName) throws IOException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try (InputStream is = classLoader.getResourceAsStream(fileName)) {
            if (is == null) return null;
            try (InputStreamReader isr = new InputStreamReader(is);
                 BufferedReader reader = new BufferedReader(isr)) {
                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }

}


