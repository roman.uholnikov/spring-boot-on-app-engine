package com.example.demo.controller;

import com.example.demo.service.DdbService;
import com.example.demo.service.HtmlProcessor;
import com.example.demo.service.S3Service;
import com.example.demo.service.SearchService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Controller
public class DiscController {
    @Autowired
    S3Service s3Service;

    @Autowired
    DdbService ddbService;

    @Autowired
    HtmlProcessor processor;

    @Autowired
    SearchService searchService;

    @GetMapping("/data/**")
    @ResponseBody
    public String home(Model model, HttpServletRequest request) {
        String fullPath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        if (fullPath.endsWith(".html")) {
            return processor.process(
                    s3Service.objectAsString(
                            fullPath.substring(1)
                    )
            );
        }
        return "What type of file??? I do not know it yet." + fullPath;
    }

    @GetMapping("/meta-info/{path}.css")
    @ResponseBody
    public String css(Model model, @PathVariable String path) {
        return s3Service.objectAsString("meta-info/{path}.css".replace("{path}", path));
    }

    @GetMapping("/meta-info/{path}.js")
    @ResponseBody
    public String js(Model model, @PathVariable String path) {
        return s3Service.objectAsString("meta-info/{path}.js".replace("{path}", path));
    }

    @GetMapping(path="/search/**", produces = "text/html; charset=utf-8")
    @ResponseBody
    public String search(@RequestParam(name = "search-path") String searchPath,
                         @RequestParam(name = "search-request", required = false) String searchRequest) throws IOException {


        Map<String, Set<String>> files = searchService.getFiles(searchPath, searchRequest);
        Map<String, Long> labels = searchService.getLabels(searchPath);

        return processor.getSearchResultHtml(files, labels, searchPath, searchRequest);
    }
}
